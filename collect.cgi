#!/usr/bin/perl -T
#!/usr/bin/perl -Tw

use strict;
use vars qw( $cgi $self_url $error
             $collect_html $collect_template
           );

use subs qw( print_redirect print_collect collect_default );
use CGI;
use Text::Template;
use Business::CreditCard;

my $redirect_url = "http://127.0.0.1/selfservice/verify.cgi";

$collect_html = -e 'collect.html'
                  ? 'collect.html'
                  : '/usr/local/freeside/collect.html';

if ( -e $collect_html ) {
  my $collect_txt = Text::Template::_load_text($collect_html)
    or die $Text::Template::ERROR;
  $collect_txt =~ /^(.*)$/s; #untaint the template source - it's trusted
  $collect_txt = $1;
  $collect_template = new Text::Template ( TYPE => 'STRING',
                                           SOURCE => $collect_txt,
                                           DELIMITERS => [ '<%=', '%>' ],
                                         )
    or die $Text::Template::ERROR;
} else {
  $collect_template = new Text::Template ( TYPE => 'STRING',
                                           SOURCE => &collect_default,
                                           DELIMITERS => [ '<%=', '%>' ],
                                         )
    or die $Text::Template::ERROR;
}

$cgi = new CGI;

if ( defined($cgi->param('magic')) && $cgi->param('magic') eq 'process' ) {

    $error = '';

    $cgi->param('paydate' => $cgi->param( 'month' ). '-'.
                             $cgi->param( 'year' )
               );

    my $payinfo = $cgi->param('payinfo');
    $payinfo =~ s/\D//g;
    $payinfo =~ /^(\d{13,16})$/ or $error ||= "Invalid card";
    $payinfo = $1;
    validate($payinfo) or $error ||= "Invalid card";

    my %rv = ( map { $_ => scalar($cgi->param($_)) } qw( reference amount ) );

    unless ( $error ) {
      $error = '_decline' unless $payinfo eq '4111111111111111';
    }
    
    if ( $error eq '_decline' ) {
      $rv{status} = '01';
      $rv{message} = 'Declined';
      print_redirect( %rv );
    } elsif ( $error eq '_collect' ) {
      print_collect();
    } elsif ( $error ) {
      print_collect();
    } else {
      $rv{status} = '00';
      $rv{message} = 'Approved';
      print_redirect( %rv );
    }

} else {
  $error = '';
  print_collect();
}

sub print_collect {

  $error = "Error: $error" if $error;

  my $r = { $cgi->Vars, 'error' => $error };

  $r->{self_url} = $cgi->self_url;

  print $cgi->header( '-expires' => 'now' ),
        $collect_template->fill_in( PACKAGE => 'FS::SelfService::_signupcgi',
                                    HASH    => $r
                                  );
}

use Data::Dumper;
sub print_redirect {
  my %param = @_;

  my $param = join('&', map { "$_=". $param{$_} } keys %param );
  warn Dumper($param);
  print $cgi->redirect( '-uri' => $redirect_url. '?'. $param );
}

sub collect_default { #html to use if there is a collect phase
  <<'END';
<HTML><HEAD><TITLE>Pay now</TITLE></HEAD>
<BODY BGCOLOR="#e8e8e8"><FONT SIZE=7>Pay now</FONT><BR><BR>
<FONT SIZE="+1" COLOR="#FF00000"><%= $error %></FONT><BR>
You are about to contact our payment processor to pay <%= $amount %> for
<%= $pkg %>.<BR><BR>
Your transaction reference number is <%= $reference %><BR><BR>
<FORM NAME="collect_popper" method="post" action="<%= $self_url %>">
<INPUT NAME="magic"  TYPE="hidden" VALUE="process">
<INPUT NAME="amount" TYPE="hidden" VALUE="<%= $amount %>">
<INPUT NAME="reference" TYPE="hidden" VALUE="<%= $reference %>">
Card Number:<INPUT NAME="payinfo" TYPE="text" VALUE=""><BR>
<INPUT NAME="submit" type="submit" value="Pay now">
</FORM>
</BODY></HTML>
END
}

# subs for the templates...

package FS::SelfService::_signupcgi;
use HTML::Entities;

